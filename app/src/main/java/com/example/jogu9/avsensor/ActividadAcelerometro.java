package com.example.jogu9.avsensor;

import android.graphics.Color;
import android.hardware.*;
import android.hardware.Sensor;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class ActividadAcelerometro extends AppCompatActivity
        implements SensorEventListener
{

    TextView datos;
    SensorManager sensorManager;
    android.hardware.Sensor acelerometro;
   int valor = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_acelerometro);

        datos = (TextView)findViewById(R.id.lblDatos);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        acelerometro = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);


        /*
        sensorEventListener= new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event)
            {
                float x= event.values[0];
                float t,y,z;
                t= event.values[0];
                y= event.values[1];
                z= event.values[2];
                datos.setText("");
                datos.append("El valor de X = "+t+ "\n El valor de Y = "+y+ "\n El valor de Z="+z);
                System.out.print("Valor de giro en x"+x);
                if (x<-5 && valor==0)
                {
                    valor++;
                    getWindow().getDecorView().setBackgroundColor(Color.BLUE);
                }else if (x>5&& valor==1)
                {
                   Toast toast = Toast.makeText( getApplicationContext(), "Se Cerro la App", Toast.LENGTH_SHORT);
                    toast.show();
                    finish();
                  //  getWindow().getDecorView().setBackgroundColor(Color.GREEN);
                  //  valor++;
                }*/


               /* if(valor==2)
                {
                    sound();
                    valor=0;
                }*/
            }

    @Override
    protected void onResume()
    {
        super.onResume();

        //Se registra el cambio del Sensor
        sensorManager.registerListener(this, acelerometro, sensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        //Se quitan los registros
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

       /* float x,y;
        x= event.values[0];
        y= event.values[1];
        datos.setText("");
        datos.append("El valor de X = "+x+ "\n El valor de Y = "+y);

        if (x < -5 || y < -5 || x > 5 || y > 5) {
            Toast toast = Toast.makeText(getApplicationContext(), "Se Cerro la App", Toast.LENGTH_SHORT);
            toast.show();
            finish();
        } else {
        }*/

        float x= event.values[0];
        float t,y,z;
        t= event.values[0];
        y= event.values[1];
        z= event.values[2];
        datos.setText("");
        datos.append("El valor de X = "+t+ "\n El valor de Y = "+y+ "\n El valor de Z="+z);
        System.out.print("Valor de giro en x"+x);
        if (x<-5 && valor==0)
        {
            valor++;
            getWindow().getDecorView().setBackgroundColor(Color.BLUE);
        }else if (x>5&& valor==1)
        {
            Toast toast = Toast.makeText(getApplicationContext(), "Se Cerro la Ventan", Toast.LENGTH_SHORT);
            toast.show();
            finish();
        }

        if(valor==2)
        {
            valor=0;
        }

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}






