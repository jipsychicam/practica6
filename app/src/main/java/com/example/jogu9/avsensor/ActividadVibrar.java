package com.example.jogu9.avsensor;

import android.content.Context;
import android.hardware.*;
import android.hardware.Sensor;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class ActividadVibrar extends AppCompatActivity
        implements SensorEventListener
{

    Button botonVibrar, botonDetener;

    Vibrator vibrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_vibrar);

        botonVibrar = (Button)findViewById(R.id.btnIniciarVibrar);
        vibrar = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        botonVibrar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                vibrar.vibrate(500);
            }
        });

        botonDetener = (Button)findViewById(R.id.btnDetener);
        botonDetener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                long[] pattern = {100, 300, 200, 100};
                vibrar.vibrate(pattern,0);
            }
        });

    }

    @Override
    public void onSensorChanged(SensorEvent event) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
