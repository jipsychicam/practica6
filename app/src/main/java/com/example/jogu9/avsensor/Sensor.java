package com.example.jogu9.avsensor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Sensor extends AppCompatActivity {

    Button botonAcelerometro, botonVibrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);

        botonAcelerometro = (Button)findViewById(R.id.btnAcelerometro);

        botonAcelerometro.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent (Sensor.this, ActividadAcelerometro.class);
                startActivity(intent);
            }
        });

        botonVibrar = (Button)findViewById(R.id.btnVibrar);

        botonVibrar.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent (Sensor.this, ActividadVibrar.class);
                startActivity(intent);
            }
        });
    }
}
